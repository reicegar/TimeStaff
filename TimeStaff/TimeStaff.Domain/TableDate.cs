﻿namespace TimeStaff.Domain
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class TableDate
    {
        [Key]
        public int TableDateId
        {
            get;
            set;
        }

        [DataType(DataType.Date)]
        public DateTime Day
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }

        [JsonIgnore]
        public virtual ICollection<TableDateTime> TableDateTimes
        {
            get;
            set;
        }
    }
}
