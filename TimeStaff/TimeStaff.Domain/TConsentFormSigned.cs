﻿namespace TimeStaff.Domain
{
    using System.ComponentModel.DataAnnotations;

    public class TConsentFormSigned
    {
        [Key]
        public int TConsentFormSignedId
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Please accept the terms and conditions.")]
        [Display(Name = "Terms and conditions")]
        public bool AcceptConsentForm
        {
            get;
            set;
        }

        public string FullName
        {
            get;
            set;
        }

        [Display(Name = "Signature")]
        [UIHint("SignaturePad")]
        public byte[] MySignature { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Signature
        {
            get;
            set;
        }

        public int TConsentFormId
        {
            get;
            set;
        }

        public bool Signed
        {
            get;
            set;
        }

        public virtual TConsentForm TConsentForm
        {
            get;
            set;
        }
    }
}
