﻿namespace TimeStaff.Domain
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TableDateTime
    {
        public int TableDateTimeId
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        [DataType(DataType.Date)]
        public DateTime Date
        {
            get;
            set;
        }

        [DataType(DataType.Time)]
        public TimeSpan StartTime
        {
            get;
            set;
        }

        [DataType(DataType.Time)]
        public TimeSpan FinishTime
        {
            get;
            set;
        }

        public virtual User user
        {
            get;
            set;
        }

        [NotMapped]
        [Display(Name = "Total")]
        public TimeSpan TotalTime
        {
            get
            {
                return FinishTime.Subtract(StartTime);
                    //string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }

        public double Total
        {
            get
            {
                return TotalTime.Hours + TotalTime.Minutes / 60.0;
            }
        }



    }
}
