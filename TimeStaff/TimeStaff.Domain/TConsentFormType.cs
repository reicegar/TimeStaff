﻿namespace TimeStaff.Domain
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class TConsentFormType
    {
        [Key]
        public int TConsentFormTypeId
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The file {0} is required")]
        [MaxLength(50, ErrorMessage = "The field {0} only can contains a maximum of {1} characters lenght.")]
        [Index("TConsentFormType_Title_Index", IsUnique = true)]
        public string Title
        {
            get;
            set;
        }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "The file {0} is required")]
        public string Comments
        {
            get;
            set;
        }


        public virtual ICollection<TConsentForm> TConsentForms
        {
            get;
            set;
        }

    }
}
