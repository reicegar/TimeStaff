﻿namespace TimeStaff.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class TConsentForm
    {
        [Key]
        public int TConsentFormId
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The file {0} is required")]
        public string Email
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The file {0} is required")]
        [Display(Name = "Full name")]
        public string FullName
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The file {0} is required")]
        [Display(Name = "Ticket Number")]
        public string TicketNumber
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The file {0} is required")]
        [Display(Name = "Company & Address")]
        public string CompanyAddress
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The file {0} is required")]
        public DateTime Date
        {
            get;
            set;
        }

        [Required(ErrorMessage = "The file {0} is required")]
        [Display(Name = "Item/s")]
        public string Items
        {
            get;
            set;
        }

        public double? Price
        {
            get;
            set;
        }

        public int TConsentFormTypeId
        {
            get;
            set;
        }

        public virtual TConsentFormType TConsentFormType
        {
            get;
            set;
        }

        public virtual ICollection<TConsentFormSigned> TConsentFormSigneds
        {
            get;
            set;
        }
          
    }
}
