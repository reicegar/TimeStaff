﻿namespace TimeStaff.Domain
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "The field {0} is requiered.")]
        [MaxLength(50, ErrorMessage = "The field {0} only can contains a maximum of {1} characters lenght.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "The field {0} is requiered.")]
        [MaxLength(50, ErrorMessage = "The field {0} only can contains a maximum of {1} characters lenght.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "The field {0} is requiered.")]
        [MaxLength(100, ErrorMessage = "The field {0} only can contains a maximum of {1} characters lenght.")]
        [Index("User_Email_Index", IsUnique = true)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [MaxLength(20, ErrorMessage = "The field {0} only can contains a maximum of {1} characters lenght.")]
        [DataType(DataType.PhoneNumber)]
        public string Telephone { get; set; }

        [JsonIgnore]
        [DataType(DataType.ImageUrl)]
        public string QrCode
        {
            get;
            set;
        }

        [Display(Name = "Employee")]
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }

        [JsonIgnore]
        [Display(Name ="Signature")]
        [UIHint("SignaturePad")]
        public byte[] MySignature { get; set; }

        
        [DataType(DataType.ImageUrl)]
        public string Signature
        {
            get;
            set;
        }

        [JsonIgnore]
        public virtual ICollection<TableDate> TableDates
        {
            get;
            set;
        }

        [JsonIgnore]
        public virtual ICollection<TableStartTime> TableStartTimes
        {
            get;
            set;
        }

        [JsonIgnore]
        public virtual ICollection<TableFinishTime> TableFinishTimes
        {
            get;
            set;
        }

        [JsonIgnore]
        public virtual ICollection<TableDateTime> TableDateTimes
        {
            get;
            set;
        }
    }
}
