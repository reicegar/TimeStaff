﻿namespace TimeStaff.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class TableStartTime
    {
        [Key]
        public int TableStartTimeId
        {
            get;
            set;
        }

        [DataType(DataType.Time)]
        public TimeSpan Timer
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }
        
    }
}
