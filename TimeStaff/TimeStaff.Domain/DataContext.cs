﻿namespace TimeStaff.Domain
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class DataContext : DbContext
    {

        public DataContext() : base("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<User> Users { get; set; }

        public System.Data.Entity.DbSet<TimeStaff.Domain.TableDate> TableDates { get; set; }

        public System.Data.Entity.DbSet<TimeStaff.Domain.TableStartTime> TableStartTimes { get; set; }

        public System.Data.Entity.DbSet<TimeStaff.Domain.TableFinishTime> TableFinishTimes { get; set; }

        public System.Data.Entity.DbSet<TimeStaff.Domain.TableDateTime> TableDateTimes { get; set; }

        public System.Data.Entity.DbSet<TimeStaff.Domain.TConsentFormType> TConsentFormTypes { get; set; }

        public System.Data.Entity.DbSet<TimeStaff.Domain.TConsentForm> TConsentForms { get; set; }

        public System.Data.Entity.DbSet<TimeStaff.Domain.TConsentFormSigned> TConsentFormSigneds { get; set; }
    }
}
