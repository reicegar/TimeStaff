﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeStaff.Domain;

namespace TimeStaff.Backend.Controllers
{
    public class TConsentFormTypesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: TConsentFormTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.TConsentFormTypes.ToListAsync());
        }

        // GET: TConsentFormTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TConsentFormType tConsentFormType = await db.TConsentFormTypes.FindAsync(id);
            if (tConsentFormType == null)
            {
                return HttpNotFound();
            }
            return View(tConsentFormType);
        }

        // GET: TConsentFormTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TConsentFormTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TConsentFormTypeId,Title,TextBody,Comments,Agree")] TConsentFormType tConsentFormType)
        {
            if (ModelState.IsValid)
            {
                db.TConsentFormTypes.Add(tConsentFormType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tConsentFormType);
        }

        // GET: TConsentFormTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TConsentFormType tConsentFormType = await db.TConsentFormTypes.FindAsync(id);
            if (tConsentFormType == null)
            {
                return HttpNotFound();
            }
            return View(tConsentFormType);
        }

        // POST: TConsentFormTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TConsentFormTypeId,Title,TextBody,Comments,Agree")] TConsentFormType tConsentFormType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tConsentFormType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tConsentFormType);
        }

        // GET: TConsentFormTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TConsentFormType tConsentFormType = await db.TConsentFormTypes.FindAsync(id);
            if (tConsentFormType == null)
            {
                return HttpNotFound();
            }
            return View(tConsentFormType);
        }

        // POST: TConsentFormTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TConsentFormType tConsentFormType = await db.TConsentFormTypes.FindAsync(id);
            db.TConsentFormTypes.Remove(tConsentFormType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
