﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeStaff.Domain;

namespace TimeStaff.Backend.Controllers
{
    public class TConsentFormSignedsController : Controller
    {
        private DataContext db = new DataContext();

        // GET: TConsentFormSigneds
        public async Task<ActionResult> Index()
        {
            var tConsentFormSigneds = db.TConsentFormSigneds.Include(t => t.TConsentForm);
            return View(await tConsentFormSigneds.ToListAsync());
        }

        // GET: TConsentFormSigneds/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TConsentFormSigned tConsentFormSigned = await db.TConsentFormSigneds.FindAsync(id);
            if (tConsentFormSigned == null)
            {
                return HttpNotFound();
            }
            return View(tConsentFormSigned);
        }

        // GET: TConsentFormSigneds/Create
        public ActionResult Create()
        {
            ViewBag.TConsentFormId = new SelectList(db.TConsentForms, "TConsentFormId", "Email");
            return View();
        }

        // POST: TConsentFormSigneds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TConsentFormSigned tConsentFormSigned)
        {
            if (ModelState.IsValid)
            {
                db.TConsentFormSigneds.Add(tConsentFormSigned);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.TConsentFormId = new SelectList(db.TConsentForms, "TConsentFormId", "Email", tConsentFormSigned.TConsentFormId);
            return View(tConsentFormSigned);
        }

        // GET: TConsentFormSigneds/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tConsentFormSigned = await db.TConsentFormSigneds.FindAsync(id);
            if (tConsentFormSigned == null)
            {
                return HttpNotFound();
            }
            ViewBag.TConsentFormId = new SelectList(db.TConsentForms, "TConsentFormId", "Email", tConsentFormSigned.TConsentFormId);
            return View(tConsentFormSigned);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TConsentFormSigned tConsentFormSigned)
        {
            if (ModelState.IsValid)
            {
                if (tConsentFormSigned.TConsentForm.FullName.ToLower() != tConsentFormSigned.FullName.ToLower())
                {
                    ModelState.AddModelError("Fullname", "Please the full name should be the same as above");
                    ViewBag.TConsentFormId = new SelectList(db.TConsentForms, "TConsentFormId", "Email", tConsentFormSigned.TConsentFormId);
                    return View(tConsentFormSigned);
                }

                if (!tConsentFormSigned.AcceptConsentForm)
                {
                    ModelState.AddModelError("AcceptConsentForm", "Please accept terms and conditions");
                    ViewBag.TConsentFormId = new SelectList(db.TConsentForms, "TConsentFormId", "Email", tConsentFormSigned.TConsentFormId);
                    return View(tConsentFormSigned);
                }

                

                tConsentFormSigned.Signed = true;
                db.Entry(tConsentFormSigned).State = EntityState.Modified;

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.TConsentFormId = new SelectList(db.TConsentForms, "TConsentFormId", "Email", tConsentFormSigned.TConsentFormId);
            return View(tConsentFormSigned);
        }

        // GET: TConsentFormSigneds/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TConsentFormSigned tConsentFormSigned = await db.TConsentFormSigneds.FindAsync(id);
            if (tConsentFormSigned == null)
            {
                return HttpNotFound();
            }
            return View(tConsentFormSigned);
        }

        // POST: TConsentFormSigneds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TConsentFormSigned tConsentFormSigned = await db.TConsentFormSigneds.FindAsync(id);
            db.TConsentFormSigneds.Remove(tConsentFormSigned);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
