﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeStaff.Domain;

namespace TimeStaff.Backend.Controllers
{
    public class TableStartTimesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: TableStartTimes
        public async Task<ActionResult> Index()
        {
            var tableStartTimes = db.TableStartTimes.Include(t => t.User);
            return View(await tableStartTimes.ToListAsync());
        }

        // GET: TableStartTimes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableStartTime tableStartTime = await db.TableStartTimes.FindAsync(id);
            if (tableStartTime == null)
            {
                return HttpNotFound();
            }
            return View(tableStartTime);
        }

        // GET: TableStartTimes/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName");
            return View();
        }

        // POST: TableStartTimes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TableStartTimeId,Timer,UserId")] TableStartTime tableStartTime)
        {
            if (ModelState.IsValid)
            {
                db.TableStartTimes.Add(tableStartTime);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableStartTime.UserId);
            return View(tableStartTime);
        }

        // GET: TableStartTimes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableStartTime tableStartTime = await db.TableStartTimes.FindAsync(id);
            if (tableStartTime == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableStartTime.UserId);
            return View(tableStartTime);
        }

        // POST: TableStartTimes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TableStartTimeId,Timer,UserId")] TableStartTime tableStartTime)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tableStartTime).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableStartTime.UserId);
            return View(tableStartTime);
        }

        // GET: TableStartTimes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableStartTime tableStartTime = await db.TableStartTimes.FindAsync(id);
            if (tableStartTime == null)
            {
                return HttpNotFound();
            }
            return View(tableStartTime);
        }

        // POST: TableStartTimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TableStartTime tableStartTime = await db.TableStartTimes.FindAsync(id);
            db.TableStartTimes.Remove(tableStartTime);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
