﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeStaff.Domain;

namespace TimeStaff.Backend.Controllers
{
    public class TableDatesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: TableDates
        public async Task<ActionResult> Index()
        {
            var tableDates = db.TableDates.Include(t => t.User);
            return View(await tableDates.ToListAsync());
        }

        // GET: TableDates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableDate tableDate = await db.TableDates.FindAsync(id);
            if (tableDate == null)
            {
                return HttpNotFound();
            }
            return View(tableDate);
        }

        // GET: TableDates/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName");
            return View();
        }

        // POST: TableDates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TableDateId,Day,UserId")] TableDate tableDate)
        {
            if (ModelState.IsValid)
            {
                db.TableDates.Add(tableDate);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableDate.UserId);
            return View(tableDate);
        }

        // GET: TableDates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableDate tableDate = await db.TableDates.FindAsync(id);
            if (tableDate == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableDate.UserId);
            return View(tableDate);
        }

        // POST: TableDates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TableDateId,Day,UserId")] TableDate tableDate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tableDate).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableDate.UserId);
            return View(tableDate);
        }

        // GET: TableDates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableDate tableDate = await db.TableDates.FindAsync(id);
            if (tableDate == null)
            {
                return HttpNotFound();
            }
            return View(tableDate);
        }

        // POST: TableDates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TableDate tableDate = await db.TableDates.FindAsync(id);
            db.TableDates.Remove(tableDate);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
