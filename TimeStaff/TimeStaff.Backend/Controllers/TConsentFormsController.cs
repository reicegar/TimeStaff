﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeStaff.Domain;

namespace TimeStaff.Backend.Controllers
{
    public class TConsentFormsController : Controller
    {
        private DataContext db = new DataContext();

        // GET: TConsentForms
        public async Task<ActionResult> Index()
        {
            var tConsentForms = db.TConsentForms.Include(t => t.TConsentFormType);
            return View(await tConsentForms.ToListAsync());
        }

        // GET: TConsentForms/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TConsentForm tConsentForm = await db.TConsentForms.FindAsync(id);
            if (tConsentForm == null)
            {
                return HttpNotFound();
            }
            return View(tConsentForm);
        }

        // GET: TConsentForms/Create
        public ActionResult Create()
        {
            ViewBag.TConsentFormTypeId = new SelectList(db.TConsentFormTypes, "TConsentFormTypeId", "Title");
            return View();
        }

        // POST: TConsentForms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TConsentForm tConsentForm)
        {
            if (ModelState.IsValid)
            {
                db.TConsentForms.Add(tConsentForm);
                await db.SaveChangesAsync();

                var tConsentFormSigned = new TConsentFormSigned
                {
                    TConsentFormId = tConsentForm.TConsentFormId
                };

                db.TConsentFormSigneds.Add(tConsentFormSigned);
                await db.SaveChangesAsync();
                
                return RedirectToAction("Index");
            }

            ViewBag.TConsentFormTypeId = new SelectList(db.TConsentFormTypes, "TConsentFormTypeId", "Title", tConsentForm.TConsentFormTypeId);
            return View(tConsentForm);
        }

        // GET: TConsentForms/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tConsentForm = await db.TConsentForms.FindAsync(id);
            if (tConsentForm == null)
            {
                return HttpNotFound();
            }
            ViewBag.TConsentFormTypeId = new SelectList(db.TConsentFormTypes, "TConsentFormTypeId", "Title", tConsentForm.TConsentFormTypeId);
            return View(tConsentForm);
        }

        // POST: TConsentForms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TConsentForm tConsentForm)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tConsentForm).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.TConsentFormTypeId = new SelectList(db.TConsentFormTypes, "TConsentFormTypeId", "Title", tConsentForm.TConsentFormTypeId);
            return View(tConsentForm);
        }

        // GET: TConsentForms/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tConsentForm = await db.TConsentForms.FindAsync(id);
            if (tConsentForm == null)
            {
                return HttpNotFound();
            }
            return View(tConsentForm);
        }

        // POST: TConsentForms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TConsentForm tConsentForm = await db.TConsentForms.FindAsync(id);
            db.TConsentForms.Remove(tConsentForm);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
