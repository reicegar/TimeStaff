﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeStaff.Domain;

namespace TimeStaff.Backend.Controllers
{
    public class TableDateTimesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: TableDateTimes
        public async Task<ActionResult> Index()
        {
            var tableDateTimes = db.TableDateTimes.Include(t => t.user);
            return View(await tableDateTimes.ToListAsync());
        }

        // GET: TableDateTimes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableDateTime tableDateTime = await db.TableDateTimes.FindAsync(id);
            if (tableDateTime == null)
            {
                return HttpNotFound();
            }
            return View(tableDateTime);
        }

        // GET: TableDateTimes/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName");
            return View();
        }

        // POST: TableDateTimes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TableDateTime tableDateTime)
        {
            if (ModelState.IsValid)
            {
                db.TableDateTimes.Add(tableDateTime);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableDateTime.UserId);
            return View(tableDateTime);
        }

        // GET: TableDateTimes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableDateTime tableDateTime = await db.TableDateTimes.FindAsync(id);
            if (tableDateTime == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableDateTime.UserId);
            return View(tableDateTime);
        }

        // POST: TableDateTimes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TableDateTime tableDateTime)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tableDateTime).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableDateTime.UserId);
            return View(tableDateTime);
        }

        // GET: TableDateTimes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableDateTime tableDateTime = await db.TableDateTimes.FindAsync(id);
            if (tableDateTime == null)
            {
                return HttpNotFound();
            }
            return View(tableDateTime);
        }

        // POST: TableDateTimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TableDateTime tableDateTime = await db.TableDateTimes.FindAsync(id);
            db.TableDateTimes.Remove(tableDateTime);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
