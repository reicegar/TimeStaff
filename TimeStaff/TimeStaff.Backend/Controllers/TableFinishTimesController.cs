﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TimeStaff.Domain;

namespace TimeStaff.Backend.Controllers
{
    public class TableFinishTimesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: TableFinishTimes
        public async Task<ActionResult> Index()
        {
            var tableFinishTimes = db.TableFinishTimes.Include(t => t.User);
            return View(await tableFinishTimes.ToListAsync());
        }

        // GET: TableFinishTimes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableFinishTime tableFinishTime = await db.TableFinishTimes.FindAsync(id);
            if (tableFinishTime == null)
            {
                return HttpNotFound();
            }
            return View(tableFinishTime);
        }

        // GET: TableFinishTimes/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName");
            return View();
        }

        // POST: TableFinishTimes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TableFinishTimeId,Timer,UserId")] TableFinishTime tableFinishTime)
        {
            if (ModelState.IsValid)
            {
                db.TableFinishTimes.Add(tableFinishTime);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableFinishTime.UserId);
            return View(tableFinishTime);
        }

        // GET: TableFinishTimes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableFinishTime tableFinishTime = await db.TableFinishTimes.FindAsync(id);
            if (tableFinishTime == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableFinishTime.UserId);
            return View(tableFinishTime);
        }

        // POST: TableFinishTimes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TableFinishTimeId,Timer,UserId")] TableFinishTime tableFinishTime)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tableFinishTime).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "FirstName", tableFinishTime.UserId);
            return View(tableFinishTime);
        }

        // GET: TableFinishTimes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TableFinishTime tableFinishTime = await db.TableFinishTimes.FindAsync(id);
            if (tableFinishTime == null)
            {
                return HttpNotFound();
            }
            return View(tableFinishTime);
        }

        // POST: TableFinishTimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TableFinishTime tableFinishTime = await db.TableFinishTimes.FindAsync(id);
            db.TableFinishTimes.Remove(tableFinishTime);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
