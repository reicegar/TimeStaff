﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TimeStaff.Backend.Startup))]
namespace TimeStaff.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
