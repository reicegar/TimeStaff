﻿using Xamarin.Forms;

[assembly: Dependency(typeof(TimeStaff.iOS.QrCodeScanningService))]

namespace TimeStaff.iOS
{
    using System.Threading.Tasks;
    using Interface;
    using ZXing.Mobile;

    public class QrCodeScanningService : IQrCodeScanningService
    {
        public async Task<string> ScanAsync()
        {
            var scanner = new MobileBarcodeScanner();
            var scanResults = await scanner.Scan();

            return scanResults.Text;
        }
    }
}