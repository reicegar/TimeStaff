﻿using Xamarin.Forms;

[assembly: Dependency(typeof(TimeStaff.Droid.QrCodeScanningService))]
namespace TimeStaff.Droid
{
    using System.Threading.Tasks;
    using Interface;
    using ZXing.Mobile;

    public class QrCodeScanningService : IQrCodeScanningService
    {
        public async Task<string> ScanAsync()
        {
            var options = new MobileBarcodeScanningOptions();
            var scanner = new MobileBarcodeScanner();
            var scanResults = await scanner.Scan(options);

            return scanResults.Text;
        }
    }
}