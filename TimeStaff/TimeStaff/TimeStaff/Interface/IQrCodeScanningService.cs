﻿using System.Threading.Tasks;

namespace TimeStaff.Interface
{
    public interface IQrCodeScanningService
    {
        Task<string> ScanAsync();
    }
}
