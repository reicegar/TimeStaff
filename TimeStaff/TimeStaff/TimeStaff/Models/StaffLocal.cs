﻿namespace TimeStaff.Models
{
    using SQLite.Net.Attributes;
    using SQLiteNetExtensions.Attributes;
    using System.Collections.Generic;

    public class StaffLocal
    {
        [PrimaryKey]
        public int UserId { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }     

        public string Signature
        {
            get;
            set;
        }

        //[OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        //public List<TableDateLocal> TableDateLocals
        //{
        //    get;
        //    set;
        //}

        //[OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        //public List<TableStartTimeLocal> TableStartTimeLocals
        //{
        //    get;
        //    set;
        //}

        //[OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        //public List<TableFinishTimeLocal> TableFinishTimeLocals
        //{
        //    get;
        //    set;
        //}

        //[OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        //public List<TableDateTimeLocal> TableDateTimeLocals
        //{
        //    get;
        //    set;
        //}

        public override int GetHashCode()
        {
            return UserId;
        }
    }
}
