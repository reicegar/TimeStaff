﻿namespace TimeStaff.Models
{
    using SQLite.Net.Attributes;
    using System;

    public class Parameter
    {
        [PrimaryKey, AutoIncrement]
        public int ParameterId
        {
            get;
            set;
        }

        public string UrlBase
        {
            get;
            set;
        }

        public override int GetHashCode()
        {
            return ParameterId;
        }
    }
}
