﻿namespace TimeStaff.Models
{
    using SQLite.Net.Attributes;
    using SQLiteNetExtensions.Attributes;
    using System;

    public  class TableFinishTimeLocal
    {
        [PrimaryKey]
        public int TableFinishTimeLocalId
        {
            get;
            set;
        }

        public TimeSpan Timer
        {
            get;
            set;
        }
      
        //[ManyToOne]
        //public StaffLocal StaffLocal
        //{
        //    get;
        //    set;
        //}

        public override int GetHashCode()
        {
            return TableFinishTimeLocalId;
        }
    }
}
