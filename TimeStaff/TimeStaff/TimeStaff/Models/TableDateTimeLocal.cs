﻿namespace TimeStaff.Models
{
    using SQLite.Net.Attributes;
    using SQLiteNetExtensions.Attributes;
    using System;

    public class TableDateTimeLocal
    {
        [PrimaryKey]
        public int TableDateTimeLocalId
        {
            get;
            set;
        }

    
        public DateTime Date
        {
            get;
            set;
        }

        public TimeSpan StartTime
        {
            get;
            set;
        }

        public TimeSpan FinishTime
        {
            get;
            set;
        }

        //[ManyToOne]
        //public StaffLocal StaffLocal
        //{
        //    get;
        //    set;
        //}

        public override int GetHashCode()
        {
            return TableDateTimeLocalId;
        }
    }
}
