﻿namespace TimeStaff.Models
{
    using SQLite.Net.Attributes;
    using SQLiteNetExtensions.Attributes;
    using System;

    public class TableStartTimeLocal
    {
        [PrimaryKey]
        public int TableStartTimeLocalId
        {
            get;
            set;
        }

        public TimeSpan Timer
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
        
        [OneToMany]
        public virtual StaffLocal StaffLocal
        {
            get;
            set;
        }

        public override int GetHashCode()
        {
            return TableStartTimeLocalId;
        }
    }
}
