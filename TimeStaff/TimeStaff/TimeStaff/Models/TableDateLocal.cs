﻿namespace TimeStaff.Models
{
    using SQLite.Net.Attributes;
    using SQLiteNetExtensions.Attributes;
    using System;
    using System.Collections.Generic;

    public class TableDateLocal
    {
        [PrimaryKey]
        public int TableDateLocalId
        {
            get;
            set;
        }

        
        public DateTime Day
        {
            get;
            set;
        }

        //[ManyToOne]
        //public virtual StaffLocal StaffLocal
        //{
        //    get;
        //    set;
        //}


        //public List<TableDateTimeLocal> TableDateTimes
        //{
        //    get;
        //    set;
        //}

        public override int GetHashCode()
        {
            return TableDateLocalId;
        }
    }
}
