﻿namespace TimeStaff.Helpers
{
    using Xamarin.Forms;
    using Interface;
    using Resources;

    public static class Languages
    {
        static Languages()
        {
            var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            Resource.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
        }

        public static string Accept
        {
            get { return Resource.Accept; }
        }

        public static string EmptyEmail
        {
            get { return Resource.EmptyEmail; }
        }

        public static string Error
        {
            get { return Resource.Error; }
        }

        public static string EmptyPassword
        {
            get { return Resource.EmptyPassword; }
        }

        public static string SomeError
        {
            get { return Resource.SomeError; }
        }

        public static string PlaceHolderEmail
        {
            get { return Resource.PlaceHolderEmail; }
        }

        public static string PlaceHolderPassword
        {
            get { return Resource.PlaceHolderPassword; }
        }

        public static string Login
        {
            get { return Resource.Login; }
        }
    }
}
