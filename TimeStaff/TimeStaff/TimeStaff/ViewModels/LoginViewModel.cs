﻿namespace TimeStaff.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Services;
    using Views;
    using Helpers;
    using System;
    using TimeStaff.Models;
    using System.Collections.Generic;

    public class LoginViewModel : BaseViewModel
    {

        #region Services

        public ApiService apiService
        {
            get;
            set;
        }

        #endregion

        #region Attributes

        private string email;

        private string password;

        private bool isRunning;

        private bool isEnabled;

        #endregion

        #region Properties

        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                SetValue(ref this.email, value);
            }
        }

        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                SetValue(ref this.password, value);
            }
        }

        public bool IsRunning
        {
            get
            {
                return this.isRunning;
            }
            set
            {
                SetValue(ref this.isRunning, value);
            }
        }

        public bool IsEnabled
        {
            get
            {
                return this.isEnabled;
            }
            set
            {
                SetValue(ref this.isEnabled, value);
            }
        }

        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }        

        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error, 
                    Languages.EmptyEmail, 
                    Languages.Accept);
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.EmptyPassword,
                    Languages.Accept);
                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;

            var connection = await apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                this.Password = string.Empty;
                this.IsRunning = false;
                this.IsEnabled = true;
                return;
            }

            var token = await this.apiService.GetToken(
                "http://vclean.somee.com",
                this.Email, 
                this.Password);
            
            if(token == null)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.SomeError,
                    Languages.Accept);
                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                await Application.Current.MainPage.DisplayAlert(
                   Languages.Error,
                   token.ErrorDescription,
                   Languages.Accept);
                this.Password = string.Empty;
                this.IsRunning = false;
                this.IsEnabled = true;
                return;
            }
            

            this.Password = string.Empty;
            this.Email = string.Empty;
            this.IsRunning = false;
            this.IsEnabled = true;

            
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token;
            mainViewModel.Scanner = new ScannerViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new ScannerPage());
        }

        

        #endregion

        #region Constructors

        public LoginViewModel()
        {
            this.apiService = new ApiService();
            this.IsEnabled = true;
        }

        #endregion

        #region Methods

        




        #endregion
    }
}
