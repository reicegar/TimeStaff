﻿namespace TimeStaff.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System;
    using System.Windows.Input;
    using Services;
    using Xamarin.Forms;
    using TimeStaff.Models;
    using TimeStaff.Helpers;
    using System.Collections.Generic;

    public class ScannerViewModel
    {
        #region Properties

        private ScanService scanService;
        private ApiService apiService;
 

        #endregion

        #region Commands

        public ICommand ScanCommand
        {
            get { return new RelayCommand(Scan); }
        }

        private async void Scan()
        {
            var test = await scanService.Scanner();

            using (var da = new DataAccess())
            {
                var staff = da.GetList<StaffLocal>(false);
                foreach (var item in staff)
                {
                    if (test == item.Email)
                    {
                        await Application.Current.MainPage.DisplayAlert(
                                            "FUCK YEAH!!!!!",
                                            "Welcome " + item.FullName,
                                            "Accept");
                        return;
                    }
                }
            }
            
            
            

            //if(test == staff.Email)
            //{
            //    await Application.Current.MainPage.DisplayAlert(
            //                        "FUCK YEAH!!!!!",
            //                        "Welcome " + staff.FullName,
            //                        "Accept");
            //    return;
            //}

            await Application.Current.MainPage.DisplayAlert(
                    "DON'T EXIST!!!!!",
                    "The user doesn't exist",
                    "Accept");
            return;
        }


        #endregion

        #region Constructor

        public ScannerViewModel()
        {
            scanService = new ScanService();
            apiService = new ApiService();
            try
            {
                LoadStaffAsync();
            }
            catch (Exception ex)
            {
                Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    ex.Message,
                    Languages.Accept);                
            }
            
        }

        #endregion

        #region Methods

        private async void LoadStaffAsync()
        {
            var connection = await apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                
                return;
            }

            var response = await apiService.GetList<StaffLocal>("http://vclean.somee.com", "/api", "/Users");

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                   Languages.Error,
                   response.Message,
                   Languages.Accept);
            }

            InsertStafflocal((List<StaffLocal>)response.Result);

        }

        private void InsertStafflocal(List<StaffLocal> staffs)
        {
            var staffLocal = new StaffLocal();
            foreach (var item in staffs)
            {
                staffLocal = new StaffLocal
                {
                    UserId = item.UserId,
                    Email = item.Email,
                    FullName = item.FullName,
                    Signature = item.Signature,
                };
                using (var da = new DataAccess())
                {
                    if(da.Find<StaffLocal>(staffLocal.UserId, false) != null)
                    {
                        da.Update(staffLocal);
                    }
                    else
                    {
                        da.Insert(staffLocal);
                    }            
                    
                }
            }

        }

        #endregion
    }
}
