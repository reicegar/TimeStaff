﻿namespace TimeStaff.Services
{
    using System;
    using System.Threading.Tasks;
    using Interface;
    using Xamarin.Forms;

    public class ScanService
    {
        public async Task<string> Scanner()
        {
            try
            {
                var scan = DependencyService.Get<IQrCodeScanningService>();
                var result = await scan.ScanAsync();
                return result.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return string.Empty;
                
            }
        }
    }
}
